package com.example.demo.repo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.db1entity;

@Repository
public interface db1repo extends JpaRepository<db1entity , Integer> {

}
